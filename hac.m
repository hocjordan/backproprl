%% HAC function
function [W, Wo, s, a, solnTimes] = hac(parameters, I, B, goal, R)

%try



% initialize variables
V = ones(parameters.S,1) * parameters.initVal; % top-level value function
Vo = ones(parameters.S,parameters.O) * parameters.initVal; % value functions for each option (cols)
W = zeros(parameters.S,parameters.A+parameters.O); % top-level action strengths. rows=states, cols=action probabilities
Wo = zeros(parameters.O,parameters.S,parameters.A); % option-specific action strenghts
solnTimes = zeros(1,parameters.episodes); % number of time steps the agent took to reach goal in each episode

for episode = 1:parameters.episodes+parameters.leadTime
    
    % if no action was specified initially, randomly select one
    if parameters.start == 0,
        startState = round(rand(1)*(parameters.S-1)) + 1;
        while (startState == goal || ismember(startState,parameters.noGoStates)),
            startState = round(rand(1)*(parameters.S-1)) + 1;
        end
    else startState = parameters.start;
    end
    
    clear a;
    clear r;
    t = 1;
    s(t) = startState; %#ok<AGROW>
    
    % continue episode until current state is the goal state or reached time limit
    while ((s(t) ~= goal && t <= parameters.maxt) || (episode <= parameters.leadTime && t <= parameters.maxt))
        
        denom = sum(exp(W(s(t),:)/parameters.tau));  % denominator of equation (1) in paper for all a's
        probs = exp(W(s(t),:)/parameters.tau)/denom;
        
        %select action probabilistically according to probs
        actionSelected = 0;
        while (actionSelected == 0)
            if parameters.withOptions
                foo = randperm(parameters.A+parameters.O); index = foo(1);
            else
                foo = randperm(parameters.A); index = foo(1);
            end
            thresh = rand(1);
            if (probs(index) >= thresh),
                if ((index <= parameters.A) || ((index > parameters.A) && (I(s(t),index-parameters.A)==1))),
                    a(t) = index;
                    actionSelected = 1;
                end
            end
        end
        
        % if a non-primitive action (option) is selected...
        if (a(t) > parameters.A)
            op = a(t) - parameters.A;
            wm = [a(t), s(t), t];
            
            % select primitive actions until subgoal is reached
            while (s(t) ~= B(op) && t < parameters.maxt)
                
                denom = sum(exp(Wo(op,s(t),:)/parameters.tau));
                probs = exp(Wo(op,s(t),:)/parameters.tau)/denom;
                
                actionSelected = 0;
                while (actionSelected == 0)
                    foo = randperm(parameters.A);
                    index = foo(1);
                    thresh = rand(1);
                    
                    if probs(index) >= thresh,
                        a(t) = index;
                        actionSelected = 1;
                    end
                end
                
                % update state
                s(t+1) = parameters.T(s(t),a(t));
                if (ismember(s(t+1),parameters.noGoStates))
                    s(t+1) = s(t);
                end
                
                % get pseudoreward if current state is a termination state for the option
                if (s(t+1) == B(op))
                    pseudor = 100;
                else
                    pseudor = 0;
                end;
                r(t+1) = pseudor; %#ok<AGROW>
                
                % compute prediction error (delta)
                delta = r(t+1) + (parameters.ngamma * Vo(s(t+1),op)) - Vo(s(t),op);
                
                % update option's value function
                Vo(s(t),op) = Vo(s(t),op) + parameters.alpha_C * delta;
                
                % update option's policy
                Wo(op,s(t),a(t)) = Wo(op,s(t),a(t)) + parameters.alpha_A * delta;
                
                if (s(t+1) == B(op) || s(t+1) == goal || t >= parameters.maxt),
                    % get real reward (but only if also at exit state of option)
                    if (s(t+1) == B(op)),
                        r(t+1) = R(s(t+1));
                    end
                    if (episode <= parameters.leadTime)
                        r(t+1) = 0;
                    end;
                    
                    % compute delta at top level
                    lag = t - wm(3);
                    initState = wm(2);
                    cumDis = parameters.ngamma^lag;
                    delta = (parameters.ngamma^(lag-1))*r(t+1)+ cumDis*(V(s(t+1))) - V(initState); %????why is it V instead of Vo???
                    
                    % update root value function (for s where option chosen)
                    V(initState) = V(initState) + parameters.alpha_C * delta;
                    
                    % update root actionStrenghts (for s where o selected)
                    W(initState,wm(1)) = W(initState,wm(1)) + parameters.alpha_A * delta;
                end
                
                t = t+1;
            end
            
            
        else % primitive action selected at root level
            
            % update state (deteriministic)
            s(t+1) = parameters.T(s(t),a(t));
            if (ismember(s(t+1),parameters.noGoStates))
                s(t+1) = s(t);
            end
            
            % get reward
            r(t+1) = R(s(t+1));
            if (episode <= parameters.leadTime)
                r(t+1) = 0;
            end
            
            % compute delta
            delta = r(t+1) + (parameters.ngamma * V(s(t+1))) - V(s(t));
            
            % update value function
            V(s(t)) = V(s(t)) + parameters.alpha_C * delta;
            
            % update policy
            W(s(t),a(t)) = W(s(t),a(t)) + parameters.alpha_A * delta;
            
            t = t+1;
            
        end % (stuff following on selection of action at root level)
        solnTimes(episode) = t;
    end % end of a single episode
    
    % update plots
    
    %{
    if (episode > parameters.leadTime)
        if parameters.S == 169 && parameters.A == 8 && parameters.O == 8
            subplot(1,4,1:2);hold on; plot (episode, t,'o'); drawnow;
            xlabel('episode')
            ylabel('number of time steps')
            theStrengths = reshape(Wo(option_for_plot,:,:),169,parameters.A);
            [vals, inds] = max (theStrengths,[],2);
            strengthsMatrix = reshape(inds,13,13)';
            
        else
            hold on; plot (episode, t,'o'); drawnow;
        end
    end
    %}
end

%{
    if parameters.S == 169 && parameters.A == 8 && parameters.O == 8
        U = zeros(1,169);
        V = zeros(1,169);
        X = zeros(1,169);
        Y = zeros(1,169);
        for i=1:13
            for j=1:13
                if ismember(j+13*(i-1),find(I(:,option_for_plot)))
                    X(i+13*(j-1)) = j;
                    Y(i+13*(j-1)) = i;
                    
                    switch strengthsMatrix(i,j)
                        case 1
                            u = 0; v = 1;
                        case 2
                            u = 1/sqrt(2); v = 1/sqrt(2);
                        case 3
                            u = 1; v = 0;
                        case 4
                            u = 1/sqrt(2); v = -1/sqrt(2);
                        case 5
                            u = 0; v=-1;
                        case 6
                            u = -1/sqrt(2); v = -1/sqrt(2);
                        case 7
                            u = -1; v=0;
                        case 8
                            u = -1/sqrt(2); v = 1/sqrt(2);
                    end
                    U(i+13*(j-1)) = u;
                    V((i+13*(j-1))) = v;
                end
            end
        end
        
        walls = ones(1,169);
        for i=1:169
            if ismember(i,parameters.noGoStates)
                walls(i) = 0;
            end
        end
        
        subplot(1,4,3:4);
        walls = reshape(walls,13,13)';
        walls(ceil(B(option_for_plot)/13),mod(B(option_for_plot),13)) = .5;
        imagesc(walls); hold on; axis square;
        xlabel(['option-specific action strengths for option ',num2str(option_for_plot),])
        quiver(X,Y,U,-V,0.25);
        ticks = 1:13;
        set(gca,'YTick',ticks)
        set(gca,'XTick',ticks)
        
        borders = 1.5:12.5;
        for i=1:12
            line([0 ; 13.5],[borders(i) ; borders(i)])
            line([borders(i) ; borders(i)], [0 ; 13.5])
        end
        
        colormap(gray);
    end
%}
%{
catch ME
    
    % display error report
    disp(getReport(ME));
    keyboard;
    
    return;
end
    
%}

end