#def ismember(a, b):
    #bind = {}
    #for i, elt in enumerate(b):
    #    if elt not in bind:
    #        bind[elt] = i
    #return [bind.get(itm, None) for itm in a]  # None can be replaced by any other "not in b" value

def ismember(A, B):
    return [ np.sum(a == B) for a in A ]