def hac(netOut, param, initialRun):

    import pdb
    import numpy as np
    from numpy import random
    from ismember import ismember
    import matplotlib.pyplot as plt
    from chunk_analyse import chunk_analyse

    print np.argmax(netOut['R'])
    #print np.where(param['T']+1 == 106)
    #print param
    # initialize variables

    V = np.ones([param['S'],1]) * param['initVal']; # top-level value function
    Vo = np.ones([param['S'],param['O']]) * param['initVal']; # value functions for each option (cols)
    solnTimes = np.zeros([1,param['episodes']]); # number of time steps the agent took to reach goal in each episode
    trackedStates = np.zeros([1,param['S']])
    W = netOut['W']
    Wo = netOut['Wo']

    for episode in range(0, param['episodes']+param['leadTime']-1):
    
        # if no state was specified initially, randomly select one
        if param['start'] == 0:
            startState = round(random.rand(1)*(param['S']-1)) + 1
            while startState == netOut['goal'] or startState in param['noGoStates']:
                startState = round(random.rand(1)*(param['S']-1)) + 1
        else:
            startState = param['start']
    
        a = {}
        r = {}; r[1] = 0; # reward not assigned for r[t=0]
        t = 1;
        s = {}
        s[t] = startState-1; ##ok<AGROW>
    



        # continue episode until current state is the goal state or reached time limit
        while ((s[t]+1 != netOut['goal'] and t <= param['maxt']) or (episode <= param['leadTime'] and t <= param['maxt'])):

            denom = sum(np.exp(W[s[t],:]/param['tau']));  # denominator of equation (1) in paper for all a's
            probs = np.exp(W[s[t],:]/param['tau'])/denom;
        




            #select action probabilistically according to probs (a is 0:8)
            actionSelected = 0;
            while (actionSelected == 0):
                if param['withOptions']:
                    #print 'withOptions ON'
                    foo = random.permutation(param['A']+param['O']) # arrange actions + options in random order
                    #print foo
                    index = foo[0] # select the first action on the list
                    #print index
                else:
                    foo = random.permutation(param['A']); # same as above, without options
                    index = foo[0]
                thresh = random.rand(1);
                if (probs[index] >= thresh):
                    #print index
                    if ((index <= param['A']-1) or ((index > param['A']-1) and (netOut['I'][s[t],index-(param['A'])]==1))): # if index is within max. actions OR outside it and the current state is an initiation state
                        #pdb.set_trace() # index = 4 REMOVE!!!!!!!!!!!!!!!!!!!!!!
                        a[t] = index;
                        #print a[t]
                        actionSelected = 1;
        




            # if a non-primitive action (option) is selected...
            if (a[t] > param['A']-1):
                #print (netOut['B'])
                op = a[t] - param['A']
                #print ('Option Selected', op, 'at time:', t, 'in state:', s[t])
                #print ('Termination state:', netOut['B'][op])
                wm = np.hstack((a[t], s[t], t))
                #print wm
            



                # select primitive actions until subgoal is reached
                while (s[t] != netOut['B'][op] and t < param['maxt']):
                
                    denom = sum(np.exp(Wo[op,s[t],:]/param['tau']));
                    probs = np.exp(Wo[op,s[t],:]/param['tau'])/denom;
                
                    actionSelected = 0;
                    while (actionSelected == 0):
                        foo = random.permutation(param['A']);
                        index = foo[0];
                        thresh = random.rand(1);
                    
                        if probs[index] >= thresh:
                            a[t] = index;
                            actionSelected = 1;
                



                    # update state
                    s[t+1] = param['T'][s[t],a[t]]-1;
                    if s[t+1] in param['noGoStates'] or s[t+1] > param['S']-1 or s[t+1] < 1:
                        s[t+1] = s[t];
                
                    # get pseudoreward if current state is a termination state for the option
                    if (s[t+1] == netOut['B'][op]):
                        #print ('Pseudoreward Time:', t)
                        #print ('Pseudoreward State:', s[t+1])
                        pseudor = 100;
                    else:
                        pseudor = 0;
                        
                    r[t+1] = pseudor; ##ok<AGROW>
                
                    # compute prediction error (delta)
                    delta = r[t+1] + (param['ngamma'] * Vo[s[t+1],op]) - Vo[s[t],op];
                
                    # update option's value function
                    Vo[s[t],op] = Vo[s[t],op] + param['alpha_C'] * delta;
                
                    # update option's policy
                    Wo[op,s[t],a[t]] = Wo[op,s[t],a[t]] + param['alpha_A'] * delta;
                
                    if s[t+1] == netOut['B'][op] or s[t+1] == netOut['goal'] or t >= param['maxt']:
                        # get real reward (but only if also at exit state of option)
                        if (s[t+1] == netOut['B'][op]):
                            r[t+1] = netOut['R'][s[t+1]];

                        if (episode <= param['leadTime']):
                            r[t+1] = 0;

                        if r[t+1] > 0:
                            print ('Reward Received: ', r[t+1], 'in state: ', s[t+1], t+1)
                    
                        # compute delta at top level
                        lag = t - wm[2]
                        initState = wm[1];
                        cumDis = np.power(param['ngamma'], lag);
                        delta = (np.power(param['ngamma'], (lag-1)))*r[t+1]+ cumDis*(V[s[t+1]]) - V[initState] #????why is it V instead of Vo???
                    
                        # update root value function (for s where option chosen)
                        V[initState] = V[initState] + param['alpha_C'] * delta;
                    
                        # update root actionStrenghts (for s where o selected)
                        W[initState,wm[0]] = W[initState,wm[0]] + param['alpha_A'] * delta;
                
                    t = t+1;

                #print ('Subgoal reached', t)
            





            
            else: # primitive action selected at root level
                
                #import pdb; pdb.set_trace()

                # update state (deteriministic)
                s[t+1] = param['T'][s[t],a[t]]-1;

                #print (s[t], a[t], param['T'][s[t]+1,a[t]], s[t+1])
                #print s[t+1] > param['S']-1 or s[t+1] < 1
                #print s[t+1] in param['noGoStates'] or s[t+1] < 1
                if s[t+1]+1 in param['noGoStates'] or s[t+1] > param['S']-1 or s[t+1] < 1:
                    #print 'noGo'
                    s[t+1] = s[t];
            
                # get reward
                r[t+1] = netOut['R'][s[t+1]];

                if (episode <= param['leadTime']):
                    r[t+1] = 0;

                if r[t+1] > 0:
                    print ('Reward Received: ', r[t+1], 'in state: ', s[t+1], t+1)
                    #pdb.set_trace()

                #pdb.set_trace();
            
                # compute delta
                delta = r[t+1] + (param['ngamma'] * V[s[t+1]]) - V[s[t]];
                #print delta
            
                # update value function
                V[s[t]] = V[s[t]] + param['alpha_C'] * delta;
            
                # update policy
                W[s[t],a[t]] = W[s[t],a[t]] + param['alpha_A'] * delta;
                
                t = t+1;
            
            solnTimes[0, episode] = t;
    
        # End of Episode
        #pdb.set_trace()


    # end of All Episodes
    plt.plot(range(1, param['episodes']), solnTimes[0][1:])

    pdb.set_trace()


    chunk_analyse(Wo, param['O'])

    
    return {'W' : W, 'Wo' : Wo, 's' : s, 'a' : a, 'solnTimes' : solnTimes}
