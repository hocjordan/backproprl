def chunk_analyse(Wo, O):

	# Import functions and arrow files
	from scipy import misc
	import numpy as np
	import pdb
	import matplotlib.pyplot as plt

	arrow_nw = misc.imread('arrow_nw.png'); arrow_nw[arrow_nw>0] = 255;
	arrow_w = misc.imread('arrow_w.png'); arrow_w[arrow_w>0] = 255;
	arrow_sw = misc.imread('arrow_sw.png'); arrow_sw[arrow_sw>0] = 255;
	arrow_n = misc.imread('arrow_n.png'); arrow_n[arrow_n>0] = 255;
	arrow_s = misc.imread('arrow_s.png'); arrow_s[arrow_s>0] = 255;
	arrow_ne = misc.imread('arrow_ne.png'); arrow_ne[arrow_ne>0] = 255;
	arrow_e = misc.imread('arrow_e.png'); arrow_e[arrow_e>0] = 255;
	arrow_se = misc.imread('arrow_se.png'); arrow_se[arrow_se>0] = 255;


	# Make array of states and walls
	walls = np.zeros([169])
	noGoStates = np.array([[1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [20], [26], [27], [33], [39], [40], [52], [53], [59], [65], [66], [72], [78], [79], [80], [82], [83], [84], [85], [91], [92], [98], [99], [100], [102], [103], [104], [105], [111], [117], [118], [124], [130], [131], [143], [144], [150], [156], [157], [158], [159], [160], [161], [162], [163], [164], [165], [166], [167], [168], [169]])
	    
	for i in np.arange(1, 170):
		if i in noGoStates:
			walls[i-1] = 255;

	pdb.set_trace()



	walls = np.reshape(walls, [13, 13])

	#plt.imshow(walls, interpolation='nearest');
	#plt.show()


	# For each option:
	for i in np.arange(0, O-1):

		# Make array
		#display = np.tile(np.zeros([4, 4]), (13, 13));
		display = np.zeros([16, 16]);
		displayArr = np.array([display for _ in range(169)])

		pdb.set_trace()

		# For each state
		for j in np.arange(0, 169):

			# Find the highest action
			#pdb.set_trace()
			if np.any(Wo[i,j,:]) != 0:

				idx = np.argmax(Wo[i,j,:])

				
				pdb.set_trace()

				#print('Max. action in Option', i, 'at state', j, 'is', idx)
				#stateIdx = np.unravel_index(j, [13, 13])
				#stateBlock = (y * 3 - 1) + ((worldSize*3) + (worldSize*3) * 3 * (x-1));
				
				if idx == 0:
					displayArr[j, :, :] = arrow_n[:,:,3]
				elif idx == 1:
					displayArr[j, :, :] = arrow_ne[:,:,3]
				elif idx == 2:
					displayArr[j, :, :] = arrow_e[:,:,3]
				elif idx == 3:
					displayArr[j, :, :] = arrow_se[:,:,3]
				elif idx == 4:
					displayArr[j, :, :] = arrow_s[:,:,3]
				elif idx == 5:
					displayArr[j, :, :] = arrow_sw[:,:,3]
				elif idx == 6:
					displayArr[j, :, :] = arrow_w[:,:,3]
				elif idx == 7:
					displayArr[j, :, :] = arrow_nw[:,:,3]
				else:
					print('INVALID ACTION:', idx)

		pdb.set_trace()

		displayArr = np.reshape(displayArr, (16*13, 16*13)) # borked

		plt.imshow(displayArr)
		plt.show()
